﻿# Rechner
[System.Double]$zahl1=Read-Host -Prompt Zahl1 
# -promt erzeugt eine Ausgabe vor der Eingabe; ersetzt write-host davor
# [double]$variable erzwingt(typisiert) das die Variable vom Typ XX ist, ist aber nicht sicher(kann zB. keine Buchstaben abfangen)

[double]$zahl2=Read-Host -Prompt Zahl2
# read-host liest immer Strings ein

$ergebnis=$zahl1 + $zahl2
Write-Output $ergebnis

