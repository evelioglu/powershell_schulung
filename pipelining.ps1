﻿# Pipelining
# sortieren
Get-Process | Sort-Object -Property CPU -Descending
# filtern
Get-Process | Where-Object -Property Handles -gt 1000
#kombiniert
Get-Process | Where-Object -Property CPU -gt 100 | Sort-Object -Property CPU -Descending
# an Variable übergeben
$prozesse=Get-Process | Where-Object -Property CPU -gt 100 | Sort-Object -Property CPU -Descending


#write-host $prozesse #funktioniert nicht, write-host benötigt einen String

$prozesse #[1] gibt nur das erste Element aus

$prozesse | Export-Csv -Path "C:\Users\MOCadmin\Desktop\ps\tst.csv" -Delimiter ";" 
# -Append hängt an statt zu überschreiben  
# delimiter setzt den csv trenner auf ";" damit Excel das format selber erkennt

$prozess_import= Import-Csv -Path "C:\Users\MOCadmin\Desktop\ps\tst.csv" -Delimiter ";" 
# importiert die csv wieder

$prozess_import