﻿#### Eingaben
# Write-Host Host kann Farben usw.
Write-Output Output "einfach"

# setze Hintergrund/Vordergrund(Schrift) FarbeWrite-Host "Test" -BackgroundColor Black -ForegroundColor Yellow 
Write-Host -object "test" -ForegroundColor Yellow -BackgroundColor Black
# lässt beide Zeilen hintereinander erscheinen
write-host "test" -NoNewline #(entfernt den Umbruch)
write-host "test2"
# gibt beides aus, getrennt mit dem Separator
Write-Host "test" "test2" -Separator " - "
# gib Sonderzeichen aus mit Backtick(neben backspace) zum escapen
$dollar="6.15"
Write-Host -Object "`$ $dollar"

#### Eingaben
# Write-Host "Eingabe: "
$eingabe=Read-Host
Write-Host $eingabe
# in der Powershell ISE bleiben Variablen nach dem ausführen des Scripts erhalten




