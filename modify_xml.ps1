﻿# setzt den Datenbankpfad
$datenbankpfad="C:\Users\MOCadmin\Desktop\ps\xml\user.xml"

# schreibt den Inhalt der XML in die Variable $myuser
[xml]$myuser=Get-Content -Path $datenbankpfad -Encoding UTF8

# $myuser.GetType() gibt den Type der Variable aus

$myuser.userliste.user[0].ort="neuer_ort" # ändert den Wert intern und nicht in der Datei

$myuser.Save($datenbankpfad) #speichert die intern geänderten Werte aus dem RAM in die Datei
# .save ist eine Methode des Objektes $myuser
# $myuser ist ein XML-Document


# hinzufügen
# $neuerUser=$myuser.userliste.user[0].Clone();#kopiert existierenden Datensatz
# $neuerUser.username="neuer_name"; ändere Werte der Variable


# irgendwas | Out-Null leitet die Ausgabe in "nichts" um

# $myuser.userliste.AppendChild($neuerUser); # hängt den geänderten Datensatz unten an die xml datei an(im RAM);append erzeugt auch eine Ausgabe

# $myuser.Save($datenbankpfad) speichert die Datei

$myuser.userliste.user[2]

