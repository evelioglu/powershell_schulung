﻿# Funktion ohne Rückgabewert (void)

function ZeigeMeldungohneParameter(){
Write-Host -Object "`nMeldung1`n" # die Funktion wird auch als commandlet angezeigt, alle commandlets sind Funktionen
}

ZeigeMeldungohneParameter
# Funktion mit Parameter und ohne Rückgabewert
function ZeigeMeldungmitparameter([string]$nachricht,[System.ConsoleColor]$farbe){ # Nachricht ist der Parameter der an die Funktion übergeben wird
Write-Host -Object "`n$nachricht`n" -ForegroundColor $farbe
}

ZeigeMeldungmitparameter -nachricht test -farbe yellow
ZeigeMeldungmitparameter -nachricht test2 -farbe black