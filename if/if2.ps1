﻿# Initialisierung der Variablen
[string]$name="undefiniert";

# Lese Name ein
$Name=Read-Host -Prompt "Name: "

# -eq; -gt, -lt oder auch -le (lesser or equal)
# not equal -ne

if ($Name -ceq "Meier") #-cXX ist dann case-sensitiv, normal ist der vergleich case-unsensitive
    {
    $ergebnis="gleich"
    }
else
    {
    $ergebnis="ungleich"
    }

Write-Host -Object ("Der Name ist " + $ergebnis + "!")
