# Initialisierung der Variablen
[double]$zahl=0;
[int]$zahl2=0;
[string]$ergebnis="undefiniert"

$zahl=Read-Host -Prompt "Zahl: "

# -eq; -gt, -lt oder auch -le (lesser or equal)
# not equal -ne

if ($zahl -gt 4)
    {
    $ergebnis="Groesser!"
    }
else
    {
    $ergebnis="Kleiner!"
    }

Write-Host -Object ("Die Zahl ist " + $ergebnis)
