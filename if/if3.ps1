[string]$name="undefiniert"

$Name=Read-Host -Prompt "Name: "

# Wenn der Name mit "m" oder "M" beginnt
# für Case-sensitiv "clike"
# es gehen auch normale Operatoren wie
# ? für ei Zeichen
# * für beliebig viele Zeichen
# Bsp.: M??er*
if ($Name -like "M*" )
    {
    $ergebnis="beginnt"
    }
else
    {
    $ergebnis="beginnt nicht"
    }

Write-Host -Object ("Der Name $ergebnis mit m")
