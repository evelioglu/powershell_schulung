﻿# Variable initialisieren
$plz="";
# Einlesen des Wertes
$Plz=Read-Host -Prompt "Plz: "

# If...
# if "regex"
# matched auf "00000" - "99999"
# auch ^[0-9][0-9][0-9][0-9][0-9]$ würde gehen
if ($plz -match "^\d{5}$")
    {
    $ergebnis="gültig"
    }
else
    {
    $ergebnis="ungültig"
    }

Write-Host -Object $ergebnis
