# 2 Möglichkeiten
# wie Browser:
$antwort_get=Invoke-WebRequest -uri https://localhost:44390/api/products -Method get # holt ein json Array als String

$antwort_get_objekt = $antwort_get.Content | ConvertFrom-Json # konvertiert das Feld "content" in json

# $antwort_get_objekt.GetType() Findet den Typ heraus

# write-host $antwort_get_objekt[1].ProductName # gibt den produktnamen des 2. Objektes aus

foreach ($objekt in $antwort_get_objekt){ # gibt von jedem Objekt den Namen aus
    $objekt.ProductName
    }
<#
Beispiel:
PS C:\Users\MOCadmin> C:\Users\MOCadmin\Desktop\ps\REST-API\REST-ansprechen.ps1
Chai
Chang
Aniseed Syrup
Chef Anton's Cajun Seasoning
#>

# besserer Ansatz, mit commandlet --> nur eine Zeile
$b_anwort_get = Invoke-RestMethod -Method get -Uri https://localhost:44390/api/products
# Invoke-RestMethod -Method
# die nicht "Rest"-Methoden schreiben in den http-header