﻿# Array anlegen was gesendet werden soll
$e=1;

# Setzt bei den ersten 10 Datensätzen den Namen auf "neuer_name"

while( $e -lt 10){
    $product = @{
            productid=$e #Variablen dürfen nicht mit '' escaped werden; productid ist auch gleichzeitg die "interne" DatensatzID
            productname='neuer_name'
            }
    $product_json = $product | ConvertTo-Json

    $post = Invoke-RestMethod -Method put -Uri https://localhost:44390/api/products/$e -body $product_json -ContentType 'application/json' # hier ist wichtig das die Url das Objekt enthält also /1 am ende der Uri!
    $e++;
    }

# gebe Antwort aus
Write-Host $post