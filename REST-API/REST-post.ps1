﻿# Array anlegen was gesendet werden soll
$product = @{
            productid='666'
            productname='Satan'
            }
# konvertiere Hashtable zu json
$product_json = $product | ConvertTo-Json

# sendet mit post die Daten aus $product_json an die API im Inhaltstyp "json"; gibt auch einen Rückgabewert der in $post ausgegeben wird
$post = Invoke-RestMethod -Method post -Uri https://localhost:44390/api/products -body $product_json -ContentType 'application/json'


# gebe Antwort aus
Write-Host $post