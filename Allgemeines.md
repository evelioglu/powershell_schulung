# PowerShell

## Infos
- bei Powershell kann und sollte, ein Befehl mit einem ";" abgeschlossen werden
  - Bei den meisten Programmiersprachen wir der Quellcode vor dem kompilieren "minifiziert", dabei werden u.A. alle Zeilenumbrüche entfernt, deswegen war es bei alten Sprachen notwendig befehle mit Semikolons abzuschließen
- Werte die nicht initialisiert sind:
  - bei Zahlen = 0
  - bei Strings = NULL

## Inkrementieren/Dekrementieren
```powershell
$zaehler++; #zählt um eins hoch
$zaehler+=2; # zählt jeweils um 2 hoch

```

## Syntaxprüfung
gibt undefinierte Variablen als Fehler zurück statt sie als "leer" zu verwenden
```
Set-StrictMode -version 2.0 
```

## Kommentare
<#
Blockommentar
#>

## Alias
`Set-Alias -name ll -value ls`
