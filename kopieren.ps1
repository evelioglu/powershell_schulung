﻿# Kopiert Datein von A nach B wenn eine Bedingung zutrifft
[string]$ordner="C:\Users\MOCadmin\Desktop\ps\test"
[int]$ausfuehren=1
[string]$ordner2="C:\Users\MOCadmin\Desktop\ps\test\backup"

# sucht alle Dateien eines Typs aus dem Ordner heraus und gibt sie aus
while ($ausfuehren -eq 1)
    {
    $dateityp=read-host -Prompt "`nDateityp zum sichern?`nOhne fuehrenden Punkt angeben."

    $files=Get-ChildItem -path $ordner 
    # $files ist ein object-array

    foreach ($datei in $files) # Datei ist ein einzelnes Element des Arrays "files"
        {
        if ($datei.extension -eq ".$dateityp")
            {
            $datei.CopyTo("$ordner2\"+$datei.Name,$true) # true erzwingt das überschreiben vorhandener Dateien 
            }
        }
    }



