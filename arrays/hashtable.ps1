﻿# Hashtable

$mitarbeiter=@{Anrede="Herr";Vorname="Hans";Nachname="Meier"};

# Hashtable_array
$mitarbeiter2=(
                @{Anrede="Herr";Vorname="Hans";Nachname="Meier"},
                @{Anrede="Herr";Vorname="Klaus";Nachname="Lala"},
                @{Anrede="Frau";Vorname="Lisa";Nachname="lulu"}
                )

write-host $mitarbeiter2[1].Vorname # gibt den Namen Klaus aus, weil Vorname des zweiten Datensatzes


foreach($i in $mitarbeiter2)
    {
    $i.Nachname # gibt alle Nachnamen aus
    }