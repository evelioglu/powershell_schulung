# Array deklarieren
$user=@(
("Hans","Meier","ort1",("kind00","kind01")),
("peter","Pan","ort2",("kind10","kind11"))

# Der Datensatz Peter ist die erste Zahl [1]
# Das Feld "Peter" = 0
# Das Feld "ort2" = 2
# Das Feld "kind11" = 01
)

# Das ist ein mehrdiemensionales Array, das hat den Vorteil gegenüber zwei Arrays mit jeweils nur 2-Dimensionen können hier sie "Zeilen" nicht auseinanderlaufen

Write-Host -Object ($user[0][3][0]); #ausgegeben wird "pan"
