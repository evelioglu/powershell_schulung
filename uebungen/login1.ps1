﻿# Prüfe ob der Login gültig ist
# Daten sind "hardcoded" in der Wenn-Abfrage
$user=Read-Host -Prompt "Nutzer"
$password=Read-Host -Prompt "Passwort"

if (($user -eq "Hans" -and $password -eq "Meier") -or ($user -eq "Peter" -and $password -eq "Pan")) # oder mit doppelten if
    {
    Write-Host -Object "Login korrekt" -ForegroundColor Green 
    }
else
    {
    Write-Host -Object "Login inkorrekt!" -ForegroundColor Yellow
    }
