﻿# Prüfe ob der Login gültig ist
# Daten sind in einem Hashtable-Array gespeichert
[int]$counter=0;

[bool]$loginok=$false;
[int]$zaehler_array=0;


$myuser=(
        @{Anrede="Herr";Nutzer="Hans";Passwort="Meier"},
        @{Anrede="Herr";Nutzer="Klaus";Passwort="Lala"},
        @{Anrede="Frau";Nutzer="Lisa";Passwort="lulu"}
        )



while ($counter -lt 3)
    {
    [string]$user=Read-Host -Prompt "Nutzer"
    [string]$password=Read-Host -Prompt "Passwort"

    foreach ($einuser in $myuser) #die Variable "einuser" steht für ein "Zeile/"Element" in dem Array "myuser"
        {
        if ($einuser.Nutzer-eq $user -and $password -ceq $einuser.Passwort) # hier muss immer das Element(einuser) geprüft werden und nicht die Menge der Elemente(myuser)
            {
            $loginok=$true
            $counter=4
            }
        }

    if ($loginok -eq $true)
       {
       Write-Host -Object "Login korrekt" -ForegroundColor Green 
       }
    else
        {
        Write-Host -Object "Login inkorrekt" -ForegroundColor Green 
        $counter++;
        }
    }
