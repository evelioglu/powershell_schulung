﻿# Funktionen - LoginCSV
# wie login4 aber hier ist die Logik in eine Funktion gepackt
########################################################################################
# Variablendeklaration
$csvpath="C:\Users\MOCadmin\Desktop\ps\csv\user.csv"
########################################################################################
# Definition Funk
function Check-LoginCSV([string]$username,[string]$password,[string]$csvpath){
    [bool]$loginok=$false;

    $myuser=Import-Csv -Path $csvpath -Delimiter ";"

    foreach ($einuser in $myuser)
        {
        if ($einuser.Username -eq $username -and $password -ceq $einuser.Password){
            $loginok=$true
            }

        }
        return $loginok
    }
########################################################################################
# Einlesen Eingaben
[string]$username=Read-Host -Prompt "Username"
[string]$password=Read-Host -Prompt "Password"
########################################################################################
# Aufruf Funktion
Check-LoginCSV -username $username -password $password -csvpath $csvpath



# mehrere Rückgabewert werden über Arrays/Hashtable oder ein Hastable-Arrayzurück gegeben
# am besten: 
# hashtable
# $mitarbeiter=@{Anrede="Herr";Vorname="Hans";Nachname="Meier"};
