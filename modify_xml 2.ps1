﻿# setzt den Datenbankpfad
$datenbankpfad="C:\Users\MOCadmin\Desktop\ps\xml\user.xml"

# schreibt den Inhalt der XML in die Variable $myuser
[xml]$myuser=Get-Content -Path $datenbankpfad -Encoding UTF8

# ändert die Werte der Variablen
$myuser.userliste.user[5].username="neuer_name2";
$myuser.userliste.user[5].password="neues_passwort2";
$myuser.userliste.user[5].ort="neuer_ort2";
$myuser.userliste.user[5].id="992"

# Speichert die Datei
$myuser.Save($datenbankpfad) 
